package net.itinajerio.api.service;

import net.itinajerio.api.entity.Album;

import java.util.List;

public interface IAlbumService {
    List<Album> buscarTodos();
    void guardar(Album album);
    void eliminar(int idAlbum);
}
