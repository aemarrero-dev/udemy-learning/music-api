package net.itinajerio.api.service;

import net.itinajerio.api.entity.Album;
import net.itinajerio.api.repository.IAlbumRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AlbumServiceImpl implements IAlbumService{

    private IAlbumRepository repo;

    public AlbumServiceImpl(IAlbumRepository repo) {
        this.repo = repo;
    }

    @Override
    public List<Album> buscarTodos() {
        return repo.findAll();
    }

    @Override
    public void guardar(Album album) {
        repo.save(album);
    }

    @Override
    public void eliminar(int idAlbum) {
        repo.deleteById(idAlbum);
    }
}
