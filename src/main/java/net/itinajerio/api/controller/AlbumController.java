package net.itinajerio.api.controller;

import net.itinajerio.api.entity.Album;
import net.itinajerio.api.service.IAlbumService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "api")
public class AlbumController {

    private IAlbumService serviceAlbum;

    public AlbumController(IAlbumService serviceAlbum) {
        this.serviceAlbum = serviceAlbum;
    }

    @GetMapping(value = "/albums", produces = "application/json")
    @ResponseBody
    public List<Album> listarAlbums() {
        return serviceAlbum.buscarTodos();
    }

    @PostMapping(value = "/albums", produces = "application/json")
    @ResponseBody
    public Album guardar(@RequestBody Album album) {
        serviceAlbum.guardar(album);
        return album;
    }

    @PutMapping(value = "/albums", produces = "application/json")
    @ResponseBody
    public Album modificar(@RequestBody Album album) {
        serviceAlbum.guardar(album);
        return album;
    }

    @DeleteMapping ("/albums/{id}")
    @ResponseBody
    public String eliminar(@PathVariable("id") int idAlbum) {
        serviceAlbum.eliminar(idAlbum);
        return "Registro Eliminado.";
    }

}
