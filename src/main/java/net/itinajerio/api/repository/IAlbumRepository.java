package net.itinajerio.api.repository;

import net.itinajerio.api.entity.Album;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IAlbumRepository extends JpaRepository<Album, Integer> {
}
